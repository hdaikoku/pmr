#ifndef PMR_EXAMPLES_WORD_COUNT_WORD_COUNT_REDUCER_H_
#define PMR_EXAMPLES_WORD_COUNT_WORD_COUNT_REDUCER_H_

#include <pmr/reducer.h>

class WordCountReducer : public pmr::Reducer<int> {
 public:
  int Reduce(const int &v1, const int &v2) const override;
};

extern "C" pmr::Reducer<int>::Type NewInstance() {
  return pmr::Reducer<int>::Type(new WordCountReducer);
}

#endif // PMR_EXAMPLES_WORD_COUNT_WORD_COUNT_REDUCER_H_
