#include "word_count_mapper.h"

WordCountMapper::KVs WordCountMapper::Map(const int64_t &key, const std::string &value) const {
  KVs kvs;

  size_t cur = 0, pos;
  while ((pos = value.find_first_of(' ', cur)) != value.npos) {
    kvs.emplace_back(std::make_pair(value.substr(cur, pos - cur), 1));
    cur = pos + 1;
  }
  kvs.emplace_back(std::make_pair(value.substr(cur, value.size() - cur), 1));

  return std::move(kvs);
}
