#ifndef PMR_EXAMPLES_WORD_COUNT_WORD_COUNT_MAPPER_H_
#define PMR_EXAMPLES_WORD_COUNT_WORD_COUNT_MAPPER_H_

#include <string>

#include <pmr/mapper.h>

class WordCountMapper : public pmr::Mapper<std::string, int, int64_t, std::string> {
 public:
  KVs Map(const int64_t &key, const std::string &value) const override;
};

extern "C" pmr::Mapper<std::string, int, int64_t, std::string>::Type NewInstance() {
  return pmr::Mapper<std::string, int, int64_t, std::string>::Type(new WordCountMapper);
}

#endif // PMR_EXAMPLES_WORD_COUNT_WORD_COUNT_MAPPER_H_
