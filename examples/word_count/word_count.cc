#include <iostream>

#include <pmr/pmr_context.h>

int main(int argc, char *argv[]) {
  if (argc < 4) {
    std::cerr << "Usage: " << argv[0] << " [text_file] [lib_mapper] [lib_reducer]" << std::endl;
    return 1;
  }

  pmr::PMRContext pc;

  auto text_file = pc.TextFile(argv[1]);
  auto mapped = text_file->Map(argv[2], argv[3]);
  auto reduced = mapped->Reduce(argv[3]);

  reduced->Print();

  return 0;
}
