# PMR

A C++ implementation of [MapReduce](https://research.google.com/archive/mapreduce.html)

## Requirements ##

* C++11 compatible compiler
* CMake 2.8+
* [jubatus-msgpack-rpc](https://github.com/jubatus/jubatus-msgpack-rpc/tree/master/cpp)
* [Intel Threading Building Blocks](https://www.threadingbuildingblocks.org/)
* [google-sparsehash](https://github.com/sparsehash/sparsehash)
* MLNX_OFED 3.3+ (to enable RDMA shuffling)

## How to Build ##
    
```
$ git clone --recursive https://hdaikoku@bitbucket.org/hdaikoku/pmr.git
$ cd pmr
$ mkdir build
$ cd build
$ cmake ..
$ make
```

To enable RDMA shuffling,
    
```
$ cmake -DPMR_USE_RDMA=ON ..
```

## Usage ##

1. Start master & worker processes:
    
    ```
    $ sbin/start-all.sh
    ```
    
2. Run your application (e.g. Word Count):

    ```
    $ sbin/pmr-submit.sh bin/word_count [text_file] [lib_mapper] [lib_reducer]
    ```
