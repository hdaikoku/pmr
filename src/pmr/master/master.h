#ifndef PMR_MASTER_MASTER_H_
#define PMR_MASTER_MASTER_H_

#include <jubatus/msgpack/rpc/server.h>
#include <tbb/concurrent_vector.h>

#include "pmr/executor_info.h"
#include "pmr/utils/logger.h"

namespace pmr {
namespace master {
class Master : public msgpack::rpc::dispatcher, public Logger {
 public:
  Master() : Logger("Master") {}

  void Listen(const std::string &addr, uint16_t port);

  void Run(size_t num_cores);

 private:
  msgpack::rpc::server rpc_server_;
  tbb::concurrent_vector<ExecutorInfo> executors_;

  void dispatch(msgpack::rpc::request req) override;

  void RegisterExecutor(msgpack::rpc::request &req);
  void RegisterApp(msgpack::rpc::request &req);
};
} // namespace master
} // namespace pmr

#endif // PMR_MASTER_MASTER_H_
