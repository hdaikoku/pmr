#include "pmr/master/master.h"

namespace pmr {
namespace master {

void Master::Listen(const std::string &addr, uint16_t port) {
  rpc_server_.serve(this);
  rpc_server_.listen(addr, port);

  LogDebug("now listening on " + addr + ":" + std::to_string(port));
}

void Master::Run(size_t num_cores) {
  rpc_server_.run(num_cores);
}

void Master::dispatch(msgpack::rpc::request req) {
  std::string method;
  req.method().convert(&method);

  try {
    if (method == "register_executor") {
      // executor registration
      RegisterExecutor(req);
    } else if (method == "register_app") {
      // application registration
      RegisterApp(req);
    } else {
      // no such methods
      req.error(msgpack::rpc::NO_METHOD_ERROR);
    }
  } catch (msgpack::type_error &e) {
    std::cerr << "ERROR: type_error" << std::endl;
    req.error(msgpack::rpc::ARGUMENT_ERROR);
  }
}

void Master::RegisterExecutor(msgpack::rpc::request &req) {
  msgpack::type::tuple<ExecutorInfo> params;
  req.params().convert(&params);

  ExecutorInfo executor = params.get<0>();

  LogInfo("registered a new Executor@" + executor.rpc_addr + ":" + std::to_string(executor.rpc_port));

  executors_.push_back(executor);

  req.result(0);
}

void Master::RegisterApp(msgpack::rpc::request &req) {
  req.result(std::vector<ExecutorInfo>(executors_.cbegin(), executors_.cend()));
}

} // namespace master
} // namespace pmr
