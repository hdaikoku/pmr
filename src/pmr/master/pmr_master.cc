#include "pmr/master/master.h"
#include "pmr/utils/arg_parser.h"

void parse_args(int argc, char *argv[], std::string &master_addr, uint16_t &master_port) {
  ArgParser parser;
  parser.AddOption('a', "addr", "Bind address (default: 0.0.0.0)", true);

  auto last_idx = parser.Parse(argc, argv);
  if (last_idx < 0 || parser.HasOpt('h') || argc - last_idx < 1) {
    // print usage
    std::cerr << "Usage: " << argv[0] << " <options> [master_port]" << std::endl;
    parser.PrintOptions();
    std::exit(EXIT_FAILURE);
  }

  master_addr = parser.HasOpt('a') ? parser.GetValStr('a') : "0.0.0.0";
  master_port = std::stoul(argv[last_idx]);
}

int main(int argc, char *argv[]) {
  std::string master_addr;
  uint16_t master_port;
  size_t num_cores;

  parse_args(argc, argv, master_addr, master_port);

  num_cores = 1;
  const char *env_num_threads = std::getenv("PMR_MASTER_CORES");
  if (env_num_threads) {
    try {
      num_cores = std::stoul(env_num_threads);
    } catch (const std::invalid_argument &e) {
      std::cerr << "invalid value for PMR_MASTER_CORES" << std::endl;
    } catch (const std::out_of_range &e) {
      std::cerr << "invalid value for PMR_MASTER_CORES" << std::endl;
    }
  }

  pmr::master::Master master;
  master.Listen(master_addr, master_port);
  master.Run(num_cores);

  return 0;
}
