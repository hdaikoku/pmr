#include "pmr/storage/simple_pair_buffer.h"

#include <msgpack.hpp>

#include "pmr/worker/pmr_env.h"

namespace pmr {
namespace storage {

template <typename K, typename V>
void SimplePairBuffer<K, V>::Insert(K &&k, V &&v) {
  key_values_.emplace_back(std::make_pair(k, v));
}

template <typename K, typename V>
void SimplePairBuffer<K, V>::Insert(std::pair<K, V> &&key_value) {
  key_values_.emplace_back(std::move(key_value));
}

template <typename K, typename V>
void SimplePairBuffer<K, V>::InsertAll(std::vector<std::pair<K, V>> &&key_values) {
  std::move(key_values.begin(), key_values.end(), std::back_inserter(key_values_));
}

template <typename K, typename V>
void SimplePairBuffer<K, V>::WriteBlocks(uint64_t partition_gid, uint64_t num_partitions) {
  std::vector<msgpack::sbuffer> bufs(num_partitions);
  auto hasher = std::hash<K>();

  for (const auto &kv : key_values_) {
    auto partition = hasher(kv.first) % num_partitions;
    msgpack::pack(&bufs[partition], kv);
  }

  auto &shuffle_service = worker::PMREnv::GetInstance().ShuffleServiceRef();
  for (uint64_t i = 0; i < num_partitions; i++) {
    auto blk_len = bufs[i].size();
    auto blk_ptr = bufs[i].release();
    shuffle_service.PutBlock(partition_gid, i, shuffle::Block(std::unique_ptr<char[]>(blk_ptr), blk_len));
  }
}

template <typename K, typename V>
void SimplePairBuffer<K, V>::ReadBlocks(uint64_t partition_gid, uint64_t num_partitions) {
  auto &shuffle_service = worker::PMREnv::GetInstance().ShuffleServiceRef();
  msgpack::unpacked unpacked;
  std::pair<K, V> received;

  for (uint64_t i = 0; i < num_partitions; i++) {
    auto blk = shuffle_service.GetBlock(i, partition_gid);
    size_t offset = 0;

    while (offset != blk.second) {
      msgpack::unpack(&unpacked, blk.first.get(), blk.second, &offset);
      unpacked.get().convert(&received);
      Insert(std::move(received));
    }
  }
}

template <typename K, typename V>
void SimplePairBuffer<K, V>::ForEach(const std::function<void(const std::pair<K, V> &)> &func) const {
  for (const auto &kv : key_values_) {
    func(kv);
  }
}

} // namespace storage
} // namespace pmr
