#ifndef PMR_STORAGE_PAIR_BUFFER_H_
#define PMR_STORAGE_PAIR_BUFFER_H_

#include <functional>
#include <vector>

namespace pmr {
namespace storage {

template <typename K, typename V>
class PairBuffer {
 public:
  PairBuffer() = default;
  virtual ~PairBuffer() = default;

  // insert a single KV pair
  virtual void Insert(K &&key, V &&value) = 0;
  virtual void Insert(std::pair<K, V> &&key_value) = 0;

  // insert in bulk
  virtual void InsertAll(std::vector<std::pair<K, V>> &&key_values) = 0;

  // serialize buffered pairs and write them to BlockManager
  virtual void WriteBlocks(uint64_t partition_gid, uint64_t num_partitions) = 0;

  // read blocks from BlockManager and deserialize them
  virtual void ReadBlocks(uint64_t partition_gid, uint64_t num_partitions) = 0;

  // apply func to each KV pair
  virtual void ForEach(const std::function<void(const std::pair<K, V> &)> &func) const = 0;

  virtual size_t size() const = 0;
};

} // namespace storage
} // namespace pmr

#endif // PMR_STORAGE_PAIR_BUFFER_H_
