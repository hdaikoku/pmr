#include "pmr/storage/combined_pair_buffer.h"

#include <msgpack.hpp>

#include "pmr/worker/pmr_env.h"

namespace pmr {
namespace storage {

template <typename K, typename V>
CombinedPairBuffer<K, V>::CombinedPairBuffer(const Reducer<V> *combiner) : combiner_(combiner) {
  key_values_.set_empty_key("");
}

template <typename K, typename V>
void CombinedPairBuffer<K, V>::Insert(K &&key, V &&value) {
  if (key_values_.find(key) != key_values_.end()) {
    // key already exists.
    // update the existing value
    key_values_[key] = combiner_->Reduce(key_values_[key], value);
  } else {
    // key does not exist.
    key_values_[key] = value;
  }
}

template <typename K, typename V>
void CombinedPairBuffer<K, V>::Insert(std::pair<K, V> &&key_value) {
  Insert(std::move(key_value.first), std::move(key_value.second));
}

template <typename K, typename V>
void CombinedPairBuffer<K, V>::InsertAll(std::vector<std::pair<K, V>> &&key_values) {
  for (auto &&kv : key_values) {
    Insert(std::move(kv));
  }
}

template <typename K, typename V>
void CombinedPairBuffer<K, V>::WriteBlocks(uint64_t partition_gid, uint64_t num_partitions) {
  std::vector<msgpack::sbuffer> bufs(num_partitions);
  auto hasher = std::hash<K>();

  for (const auto &kv : key_values_) {
    auto partition = hasher(kv.first) % num_partitions;
    msgpack::pack(&bufs[partition], kv);
  }

  auto &shuffle_service = worker::PMREnv::GetInstance().ShuffleServiceRef();
  for (uint64_t i = 0; i < num_partitions; i++) {
    auto blk_len = bufs[i].size();
    auto blk_ptr = bufs[i].release();
    shuffle_service.PutBlock(partition_gid, i, shuffle::Block(std::unique_ptr<char[]>(blk_ptr), blk_len));
  }
}

template <typename K, typename V>
void CombinedPairBuffer<K, V>::ReadBlocks(uint64_t partition_gid, uint64_t num_partitions) {
  auto &shuffle_service = worker::PMREnv::GetInstance().ShuffleServiceRef();
  msgpack::unpacked unpacked;
  std::pair<K, V> received;

  for (uint64_t i = 0; i < num_partitions; i++) {
    auto blk = shuffle_service.GetBlock(i, partition_gid);
    size_t offset = 0;

    while (offset != blk.second) {
      msgpack::unpack(&unpacked, blk.first.get(), blk.second, &offset);
      unpacked.get().convert(&received);
      Insert(std::move(received));
    }
  }
}

template <typename K, typename V>
void CombinedPairBuffer<K, V>::ForEach(const std::function<void(const std::pair<K, V> &)> &func) const {
  for (const auto &kv : key_values_) {
    func(kv);
  }
}

} // namespace storage
} // namespace pmr
