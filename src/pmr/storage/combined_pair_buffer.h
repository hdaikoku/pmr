#ifndef PMR_STORAGE_COMBINED_PAIR_BUFFER_H_
#define PMR_STORAGE_COMBINED_PAIR_BUFFER_H_

#include <google/dense_hash_map>

#include "pmr/reducer.h"
#include "pmr/storage/pair_buffer.h"

namespace pmr {
namespace storage {

template <typename K, typename V>
class CombinedPairBuffer : public PairBuffer<K, V> {
 public:
  explicit CombinedPairBuffer(const Reducer<V> *combiner);

  void Insert(K &&key, V &&value) override;

  void Insert(std::pair<K, V> &&key_value) override;

  void InsertAll(std::vector<std::pair<K, V>> &&key_values) override;

  void WriteBlocks(uint64_t partition_gid, uint64_t num_partitions) override;

  void ReadBlocks(uint64_t partition_gid, uint64_t num_partitions) override;

  void ForEach(const std::function<void(const std::pair<K, V> &)> &func) const override;

  size_t size() const override { return key_values_.size(); }

 private:
  google::dense_hash_map<K, V> key_values_;
  const Reducer<V> *combiner_;
};

} // namespace storage
} // namespace pmr

#include "pmr/storage/combined_pair_buffer.cc"

#endif // PMR_STORAGE_COMBINED_PAIR_BUFFER_H_
