#include "pmr/pmr_context.h"

#include <fstream>

#include "pmr/text_file_index.h"

namespace pmr {

PMRContext::PMRContext() : Logger("PMRContext"), new_rdd_id_(0) {
  const char *env_master_addr = std::getenv("PMR_MASTER_HOST");
  if (env_master_addr) {
    master_addr_ = env_master_addr;
  } else {
    LogError("set PMR_MASTER_HOST before in advance");
    std::exit(EXIT_FAILURE);
  }

  const char *env_master_port = std::getenv("PMR_MASTER_PORT");
  if (env_master_port) {
    master_port_ = std::stoi(env_master_port);
  } else {
    LogError("set PMR_MASTER_PORT before in advance");
    std::exit(EXIT_FAILURE);
  }

  auto executors = MasterRPC("register_app").get<std::vector<ExecutorInfo>>();

  uint64_t executor_id = 0;
  for (const auto &executor : executors) {
    executors_.emplace(std::make_pair(executor_id++, executor));
  }

  std::unordered_map<uint64_t, msgpack::rpc::future> futures;
  for (const auto &executor : executors_) {
    futures.emplace(std::make_pair(executor.first, ExecutorRPC(executor.first, "register_app", executor.first)));
  }

  for (auto &f : futures) {
    if (f.second.get<int>() != 0) {
      LogError("failed to register with Executor@"
                   + executors_[f.first].rpc_addr + ":" + std::to_string(executors_[f.first].rpc_port));
      executors_.erase(f.first);
    } else {
      LogInfo("successfully registered with Executor@"
                  + executors_[f.first].rpc_addr + ":" + std::to_string(executors_[f.first].rpc_port));
    }
  }

  futures.clear();
  for (const auto &executor : executors_) {
    SetTimeout(executor.first, 6000);
    futures.emplace(std::make_pair(executor.first, ExecutorRPC(executor.first, "sync_executors", executors_)));
  }
  for (auto &f : futures) {
    if (f.second.get<int>() != 0) {
      LogError("failed to sync executors' info with Executor@"
                   + executors_[f.first].rpc_addr + ":" + std::to_string(executors_[f.first].rpc_port));
    }
  }
}

PMRContext::~PMRContext() {
  std::vector<msgpack::rpc::future> futures;

  for (const auto &executor : executors_) {
    futures.emplace_back(ExecutorRPC(executor.first, "deregister_app"));
  }
  // futures.push_back(MasterRPC("deregister_app"));

  for (auto &f : futures) {
    f.get<int>();
  }

  sp_.end();
}

std::unique_ptr<rdd::RDDStub> PMRContext::TextFile(const std::string &file_path) {
  std::unordered_map<uint64_t, std::vector<TextFileIndex>> indices;
  auto next_executor = 0;
  auto num_executors = executors_.size();
  auto partition_size = (1 << 25);
  const char *env_partition_size = std::getenv("PMR_PARTITION_SIZE_MB");
  if (env_partition_size) {
    try {
      partition_size = std::stoi(env_partition_size) * (1 << 20);
    } catch (const std::invalid_argument &e) {
      LogError("invalid value for PMR_PARTITION_SIZE_MB");
    } catch (const std::out_of_range &e) {
      LogError("out of range value for PMR_PARTITION_SIZE_MB");
    }
  }

  std::ifstream ifs(file_path);
  if (!ifs) {
    LogError("failed to open file: " + file_path);
    return nullptr;
  }
  int64_t file_size = ifs.seekg(0, ifs.end).tellg();
  ifs.seekg(0, ifs.beg);

  std::unique_ptr<rdd::RDDStub> rdd(new rdd::RDDStub(*this));
  uint64_t num_partitions = 0;
  while (!ifs.eof()) {
    uint64_t owner = next_executor++ % num_executors;
    int64_t offset = ifs.tellg();

    if ((file_size - offset) < partition_size) {
      if (file_size > offset) {
        indices[owner].emplace_back(num_partitions, offset, (file_size - offset));
        rdd->SetPartitionOwner(owner, num_partitions++);
      }
      break;
    }

    ifs.seekg(partition_size, ifs.cur);

    if (!ifs.eof()) {
      ifs.ignore(partition_size, '\n');
    }
    int64_t end = ifs.tellg();

    indices[owner].emplace_back(num_partitions, offset, (end - offset));
    rdd->SetPartitionOwner(owner, num_partitions++);
  }
  ifs.close();
  rdd->SetNumPartitions(num_partitions);

  std::vector<msgpack::rpc::future> futures;
  for (const auto &idx : indices) {
    futures.emplace_back(ExecutorRPC(idx.first, "text_file", rdd->GetID(), rdd->GetContext(), file_path, idx.second));
  }

  for (auto &f : futures) {
    if (f.get<int>()) {
      LogError("failed to distribute file: " + file_path);
      // FIXME: re-distribute failed partition
      return nullptr;
    }
  }

  return std::move(rdd);
}

} // namespace pmr
