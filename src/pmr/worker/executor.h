#ifndef PMR_WORKER_EXECUTOR_H_
#define PMR_WORKER_EXECUTOR_H_

#include <jubatus/msgpack/rpc/server.h>
#define TBB_PREVIEW_GLOBAL_CONTROL 1
#include <tbb/global_control.h>

#include "pmr/rdd/rdd.h"
#include "pmr/rdd/rdd_context.h"
#include "pmr/utils/logger.h"
#include "pmr/executor_info.h"

namespace pmr {
namespace worker {

class Executor : public msgpack::rpc::dispatcher, public Logger {
 public:
  Executor(const std::string &master_addr, uint16_t master_port, size_t num_cores);

  void Listen(const std::string &bind_addr, uint16_t bind_port);

  void Run();

 private:
  uint64_t executor_id_;
  ExecutorInfo executor_info_;
  std::string master_addr_;
  uint16_t master_port_;
  msgpack::rpc::server rpc_server_;
  tbb::global_control tbb_control_;
  std::unordered_map<int, std::vector<std::unique_ptr<rdd::RDD>>> rdds_;

  template <typename P0>
  static void ParseParams(msgpack::rpc::request &req, P0 &p0) {
    msgpack::type::tuple<P0> params;

    req.params().convert(&params);
    p0 = params.template get<0>();
  }

  template <typename P0, typename P1, typename P2>
  static void ParseParams(msgpack::rpc::request &req, P0 &p0, P1 &p1, P2 &p2) {
    msgpack::type::tuple<P0, P1, P2> params;

    req.params().convert(&params);
    p0 = params.template get<0>();
    p1 = params.template get<1>();
    p2 = params.template get<2>();
  }

  template <typename P0, typename P1, typename P2, typename P3>
  static void ParseParams(msgpack::rpc::request &req, P0 &p0, P1 &p1, P2 &p2, P3 &p3) {
    msgpack::type::tuple<P0, P1, P2, P3> params;

    req.params().convert(&params);
    p0 = params.template get<0>();
    p1 = params.template get<1>();
    p2 = params.template get<2>();
    p3 = params.template get<3>();
  }

  template <typename P0, typename P1, typename P2, typename P3, typename P4>
  static void ParseParams(msgpack::rpc::request &req, P0 &p0, P1 &p1, P2 &p2, P3 &p3, P4 &p4) {
    msgpack::type::tuple<P0, P1, P2, P3, P4> params;

    req.params().convert(&params);
    p0 = params.template get<0>();
    p1 = params.template get<1>();
    p2 = params.template get<2>();
    p3 = params.template get<3>();
    p4 = params.template get<4>();
  }

  void dispatch(msgpack::rpc::request req) override;

  int RegisterWithMaster();

  int RegisterApplication(msgpack::rpc::request &req);
  int SyncExecutors(msgpack::rpc::request &req);
  int TextFile(msgpack::rpc::request &req);
  int MapRDD(msgpack::rpc::request &req);
  int ReduceRDD(msgpack::rpc::request &req);
  int ShuffleRDD(msgpack::rpc::request &req);
  int PrintRDD(msgpack::rpc::request &req);
  int RemoveRDD(msgpack::rpc::request &req);
  int DeregisterApp(msgpack::rpc::request &req);
};

} // namespace worker
} // namespace pmr

#endif // PMR_WORKER_EXECUTOR_H_
