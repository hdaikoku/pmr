#include "pmr/worker/executor.h"

#include <netdb.h>

#include <jubatus/msgpack/rpc/client.h>
#include <tbb/tbb.h>

#include "pmr/text_file_index.h"
#include "pmr/rdd/udf.h"
#include "pmr/rdd/text_file_rdd.h"
#include "pmr/shuffle/in_memory_shuffle_service.h"

namespace pmr {
namespace worker {

Executor::Executor(const std::string &master_addr, uint16_t master_port, size_t num_cores)
    : Logger("Executor"), master_addr_(master_addr), master_port_(master_port),
      tbb_control_(tbb::global_control::max_allowed_parallelism, num_cores) {
  executor_info_.num_cores = num_cores;
}

void Executor::Listen(const std::string &bind_addr, uint16_t bind_port) {
  if (bind_port == 0) {
    // pick a free port
    // FIXME: picked port might have been occupied by another process before performing "listen".
    struct addrinfo hints;
    std::memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_socktype = SOCK_STREAM;

    struct addrinfo *results;
    if (getaddrinfo(bind_addr.c_str(), std::to_string(bind_port).c_str(), &hints, &results)) {
      return;
    }

    int sock_fd = socket(results->ai_family, results->ai_socktype, 0);
    if (sock_fd < 0) {
      freeaddrinfo(results);
      return;
    }

    if (bind(sock_fd, results->ai_addr, results->ai_addrlen) < 0) {
      close(sock_fd);
      freeaddrinfo(results);
      return;
    }

    struct sockaddr_in saddr;
    socklen_t len = sizeof(saddr);
    if (getsockname(sock_fd, reinterpret_cast<struct sockaddr *>(&saddr), &len) < 0) {
      close(sock_fd);
      freeaddrinfo(results);
      return;
    }

    bind_port = ntohs(saddr.sin_port);

    close(sock_fd);
    freeaddrinfo(results);
  }

  rpc_server_.serve(this);
  rpc_server_.listen(bind_addr, bind_port);

  executor_info_.rpc_addr = bind_addr;
  executor_info_.rpc_port = bind_port;

  LogInfo("now listening on " + bind_addr + ":" + std::to_string(bind_port));

  auto shuffle_mode = rnetlib::Mode::SOCKET;
  const char *env_shuffle_mode = std::getenv("PMR_SHUFFLE_MODE");
  if (env_shuffle_mode) {
    std::string str_shuffle_mode(env_shuffle_mode);
    if (str_shuffle_mode == "VERBS") {
#ifdef RNETLIB_USE_RDMA
      shuffle_mode = rnetlib::Mode::VERBS;
#else
      LogError("VERBS shuffling is only available for PMR built with PMR_USE_RDMA option. "
                   "Falling back to SOCKET shuffling.");
#endif // RNETLIB_USE_RDMA
    } else if (str_shuffle_mode == "SOCKET") {
      shuffle_mode = rnetlib::Mode::SOCKET;
    } else {
      LogError("invalid value for PMR_SHUFFLE_MODE. Falling back to SOCKET shuffling.");
    }
  }

  PMREnv::GetInstance().RegisterShuffleService(
      std::unique_ptr<shuffle::ShuffleService>(new shuffle::InMemoryShuffleService(bind_addr, 0, shuffle_mode))
  );
  executor_info_.shuffle_port = PMREnv::GetInstance().ShuffleServiceRef().GetBindPort();
}

void Executor::Run() {
  rpc_server_.set_pool_size_limit(2);

  if (RegisterWithMaster()) {
    LogError("failed to register with Master@" + master_addr_ + ":" + std::to_string(master_port_));
    return;
  }
  LogDebug("successfully registered with Master@" + master_addr_ + ":" + std::to_string(master_port_));

  rpc_server_.run(1);
}

void Executor::dispatch(msgpack::rpc::request req) {
  std::string method;
  req.method().convert(&method);

  try {
    if (method == "register_app") {
      // registeration request from application
      req.result(RegisterApplication(req));
    } else if (method == "sync_executors") {
      // sync executors' info
      LogDebug("SyncExecutors RPC");
      req.result(SyncExecutors(req));
    } else if (method == "text_file") {
      // new RDD from text file
      LogDebug("TextFile RPC");
      req.result(TextFile(req));
    } else if (method == "map_rdd") {
      // map RDD
      LogDebug("MapRDD RPC");
      req.result(MapRDD(req));
    } else if (method == "reduce_rdd") {
      // map RDD
      LogDebug("ReduceRDD RPC");
      req.result(ReduceRDD(req));
    } else if (method == "shuffle_rdd") {
      // shuffle RDD
      LogDebug("ShuffleRDD RPC");
      req.result(ShuffleRDD(req));
    } else if (method == "print_rdd") {
      // print RDD
      LogDebug("PrintRDD RPC");
      req.result(PrintRDD(req));
    } else if (method == "remove_rdd") {
      // remove RDD
      LogDebug("RemoveRDD RPC");
      req.result(RemoveRDD(req));
    } else if (method == "deregister_app") {
      // de-registration of application
      LogDebug("DeregisterApp RPC");
      req.result(DeregisterApp(req));
    } else {
      // no such methods
      LogError("Invalid RPC: " + method);
      req.error(msgpack::rpc::NO_METHOD_ERROR);
    }
  } catch (msgpack::type_error &e) {
    LogError(e.what());
    req.error(msgpack::rpc::ARGUMENT_ERROR);
  }
}

int Executor::RegisterWithMaster() {
  return rpc_server_.get_session(master_addr_, master_port_).call("register_executor", executor_info_).get<int>();
}

int Executor::RegisterApplication(msgpack::rpc::request &req) {
  ParseParams(req, executor_id_);
  LogInfo("registered with an application as Executor" + std::to_string(executor_id_));
  return 0;
}

int Executor::SyncExecutors(msgpack::rpc::request &req) {
  std::unordered_map<uint64_t, ExecutorInfo> executors;
  ParseParams(req, executors);
  PMREnv::GetInstance().RegisterExecutors(std::move(executors));
  PMREnv::GetInstance().ShuffleServiceRef().Init(executor_id_);

  return 0;
}

int Executor::TextFile(msgpack::rpc::request &req) {
  int rdd_id;
  std::shared_ptr<rdd::RDDContext> rdd_context(new rdd::RDDContext);
  std::string file_path;
  std::vector<TextFileIndex> indices;

  ParseParams(req, rdd_id, *rdd_context, file_path, indices);

  auto num_local_partitions = rdd_context->partitions_by_executor[executor_id_].size();
  rdds_.emplace(std::make_pair(rdd_id, std::vector<std::unique_ptr<rdd::RDD>>(num_local_partitions)));

  for (uint64_t lid = 0; lid < num_local_partitions; lid++) {
    const auto &idx = indices[lid];
    rdds_[rdd_id][lid] = std::unique_ptr<rdd::TextFileRDD>(
        new rdd::TextFileRDD(rdd_context, idx.partition_id_, lid, file_path, idx)
    );
  }

  return 0;
}

int Executor::MapRDD(msgpack::rpc::request &req) {
  int rdd_id, new_rdd_id;
  std::string dl_mapper, dl_combiner;
  ParseParams(req, rdd_id, new_rdd_id, dl_mapper, dl_combiner);

  // load mapper library
  // TODO dirty hack :)
  rdd::UDF udf_mapper(dl_mapper);
  auto mapper = udf_mapper.Load<Mapper<std::string, int, long long int, std::string>>();
  if (mapper == nullptr) {
    LogError("failed to load user-defined Map function from: " + dl_mapper);
    return 1;
  }

  // load combiner library
  // TODO dirty hack :)
  rdd::UDF udf_combiner(dl_combiner);
  auto combiner = udf_combiner.Load<Reducer<int>>();

  auto &rdds = rdds_[rdd_id];
  rdds_.emplace(std::make_pair(new_rdd_id, std::vector<std::unique_ptr<rdd::RDD>>(rdds.size())));

  // setup shuffle buffer
  PMREnv::GetInstance().ShuffleServiceRef().InitBlockBuffer(rdds[0]->GetNumPartitions(),
                                                            rdds[0]->GetLocalGPIDs(executor_id_));

  tbb::parallel_for_each(rdds.begin(), rdds.end(), [&](const std::unique_ptr<rdd::RDD> &rdd) {
    rdds_[new_rdd_id][rdd->GetLPID()]
        = dynamic_cast<rdd::TextFileRDD *>(rdd.get())->Map(mapper.get(), combiner.get());
  });

  return 0;
}

int Executor::ReduceRDD(msgpack::rpc::request &req) {
  int rdd_id, new_rdd_id;
  std::string dl_reducer;
  ParseParams(req, rdd_id, new_rdd_id, dl_reducer);

  // load reducer library
  // TODO dirty hack :)
  rdd::UDF udf_reducer(dl_reducer);
  auto reducer = udf_reducer.Load<Reducer<int>>();
  if (!reducer) {
    LogError("failed to load user-defined Reduce function from: " + dl_reducer);
    return 1;
  }

  auto &rdds = rdds_[rdd_id];
  rdds_.emplace(std::make_pair(new_rdd_id, std::vector<std::unique_ptr<rdd::RDD>>(rdds.size())));

  tbb::parallel_for_each(rdds.begin(), rdds.end(), [&](const std::unique_ptr<rdd::RDD> &rdd) {
    rdds_[new_rdd_id][rdd->GetLPID()]
        = static_cast<rdd::KeyValuesRDD<std::string, int> *>(rdd.get())->Reduce(reducer.get());
  });

  return 0;
}

int Executor::ShuffleRDD(msgpack::rpc::request &req) {
  int rdd_id;
  ParseParams(req, rdd_id);

  PMREnv::GetInstance().ShuffleServiceRef().Start(rdds_[rdd_id][0]->GetContext());

  return 0;
}

int Executor::PrintRDD(msgpack::rpc::request &req) {
  int rdd_id;
  ParseParams(req, rdd_id);

  auto &rdds = rdds_[rdd_id];
  for (const auto &rdd : rdds) {
    rdd->Print();
  }

  return 0;
}

int Executor::RemoveRDD(msgpack::rpc::request &req) {
  int rdd_id;
  ParseParams(req, rdd_id);

  rdds_.erase(rdd_id);

  return 0;
}

int Executor::DeregisterApp(msgpack::rpc::request &req) {
  return 0;
}

} // namespace worker
} // namespace pmr
