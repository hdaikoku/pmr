#ifndef PMR_WORKER_PMR_ENV_H_
#define PMR_WORKER_PMR_ENV_H_

#include "pmr/executor_info.h"
#include "pmr/shuffle/shuffle_service.h"

namespace pmr {
namespace worker {

class PMREnv {
 public:
  PMREnv(const PMREnv &) = delete;
  PMREnv(PMREnv &&) = delete;

  PMREnv &operator=(const PMREnv &) = delete;

  static PMREnv &GetInstance();

  void RegisterShuffleService(std::unique_ptr<shuffle::ShuffleService> shuffle_service);

  void RegisterExecutors(std::unordered_map<uint64_t, ExecutorInfo> executors);

  shuffle::ShuffleService &ShuffleServiceRef();

  const std::unordered_map<uint64_t, ExecutorInfo> &ExecutorsRef() const;

 private:
  PMREnv() = default;
  virtual ~PMREnv() = default;

  std::unique_ptr<shuffle::ShuffleService> shuffle_service_;
  std::unordered_map<uint64_t, ExecutorInfo> executors_;
};

} // namespace worker
} // namespace pmr

#endif // PMR_WORKER_PMR_ENV_H_
