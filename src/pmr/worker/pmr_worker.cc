#include <tbb/task_scheduler_init.h>

#include "pmr/utils/arg_parser.h"
#include "pmr/worker/executor.h"

void parse_args(int argc, char *argv[], std::string &bind_addr, uint16_t &bind_port,
                std::string &master_addr, uint16_t &master_port) {
  ArgParser parser;
  parser.AddOption('a', "bind_addr", "Bind address (default: 127.0.0.1)", true);
  parser.AddOption('p', "bind_port", "Bind port (default: random free port)", true);

  auto last_idx = parser.Parse(argc, argv);
  if (last_idx < 0 || parser.HasOpt('h') || argc - last_idx < 2) {
    // print usage
    std::cerr << "Usage: " << argv[0] << " <options> [master_addr] [master_port]" << std::endl;
    parser.PrintOptions();
    std::exit(EXIT_FAILURE);
  }

  bind_addr = parser.HasOpt('a') ? parser.GetValStr('a') : "localhost";
  bind_port = parser.HasOpt('p') ? std::stoul(parser.GetValStr('p')) : 0;
  master_addr = argv[last_idx];
  master_port = std::stoul(argv[last_idx + 1]);
}

int main(int argc, char *argv[]) {
  std::string bind_addr;
  uint16_t bind_port;
  std::string master_addr;
  uint16_t master_port;
  size_t num_cores;

  parse_args(argc, argv, bind_addr, bind_port, master_addr, master_port);

  num_cores = static_cast<size_t>(tbb::task_scheduler_init::default_num_threads());
  const char *env_num_threads = std::getenv("PMR_WORKER_CORES");
  if (env_num_threads) {
    try {
      num_cores = std::stoul(env_num_threads);
    } catch (const std::invalid_argument &e) {
      std::cerr << "invalid value for PMR_WORKER_CORES" << std::endl;
    } catch (const std::out_of_range &e) {
      std::cerr << "invalid value for PMR_WORKER_CORES" << std::endl;
    }
  }

  pmr::worker::Executor executor(master_addr, master_port, num_cores);
  executor.Listen(bind_addr, bind_port);
  executor.Run();

  return 0;
}

