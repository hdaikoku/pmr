#include "pmr/worker/pmr_env.h"

namespace pmr {
namespace worker {

PMREnv &PMREnv::GetInstance() {
  static PMREnv pmr_env;
  return pmr_env;
}

void PMREnv::RegisterShuffleService(std::unique_ptr<shuffle::ShuffleService> shuffle_service) {
  shuffle_service_ = std::move(shuffle_service);
}

void PMREnv::RegisterExecutors(std::unordered_map<uint64_t, ExecutorInfo> executors) {
  executors_ = std::move(executors);
}

shuffle::ShuffleService &PMREnv::ShuffleServiceRef() { return *shuffle_service_; }

const std::unordered_map<uint64_t, ExecutorInfo> &PMREnv::ExecutorsRef() const { return executors_; }

} // namespace worker
} // namespace pmr
