#ifndef PMR_EXECUTOR_INFO_H_
#define PMR_EXECUTOR_INFO_H_

#include <msgpack.hpp>

namespace pmr {

struct ExecutorInfo {
  std::string rpc_addr;
  uint16_t rpc_port;
  uint16_t shuffle_port;
  size_t num_cores;

  MSGPACK_DEFINE(rpc_addr, rpc_port, shuffle_port, num_cores);
};

} // namespace pmr

#endif // PMR_EXECUTOR_INFO_H_
