#ifndef PMR_SHUFFLE_SHUFFLE_SERVICE_H_
#define PMR_SHUFFLE_SHUFFLE_SERVICE_H_

#include <memory>
#include <vector>

#include "pmr/rdd/rdd_context.h"

namespace pmr {
namespace shuffle {

using Block = std::pair<std::unique_ptr<char[]>, size_t>;

class ShuffleService {
 public:
  ShuffleService() = default;
  virtual ~ShuffleService() = default;

  virtual void Init(uint64_t executor_id) = 0;

  virtual void InitBlockBuffer(uint64_t num_partitions, const std::vector<uint64_t> &local_gpids) = 0;

  virtual int Start(const rdd::RDDContext &rdd_ctx) = 0;

  virtual void PutBlock(uint64_t src_gpid, uint64_t dst_gpid, Block blk) = 0;

  virtual Block GetBlock(uint64_t src_gpid, uint64_t dst_gpid) = 0;

  virtual uint16_t GetBindPort() const = 0;
};

} // namespace shuffle
} // namespace pmr

#endif // PMR_SHUFFLE_SHUFFLE_SERVICE_H_
