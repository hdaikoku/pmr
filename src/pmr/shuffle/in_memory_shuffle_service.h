#ifndef PMR_SHUFFLE_IN_MEMORY_SHUFFLE_SERVICE_H_
#define PMR_SHUFFLE_IN_MEMORY_SHUFFLE_SERVICE_H_

#include <unordered_map>
#include <vector>

#include <rnetlib/rnetlib.h>

#include "pmr/shuffle/shuffle_service.h"
#include "pmr/utils/logger.h"

namespace pmr {
namespace shuffle {

class InMemoryShuffleService : public ShuffleService, public Logger {
 public:
  InMemoryShuffleService(const std::string &bind_addr, uint16_t bind_port, rnetlib::Mode mode);
  ~InMemoryShuffleService() override = default;

  void Init(uint64_t executor_id) override;

  void InitBlockBuffer(uint64_t num_partitions, const std::vector<uint64_t> &local_gpids) override;

  int Start(const rdd::RDDContext &rdd_ctx) override;

  void PutBlock(uint64_t src_gpid, uint64_t dst_gpid, Block blk) override;

  Block GetBlock(uint64_t src_gpid, uint64_t dst_gpid) override;

  uint16_t GetBindPort() const override;

 private:
  uint64_t executor_id_;
  uint64_t num_partitions_;
  std::unordered_map<uint64_t, uint64_t> gpid2lpid_;
  std::vector<Block> blocks_;
  rnetlib::Mode mode_;
  rnetlib::Server::ptr shuffle_server_;
  std::unordered_map<uint64_t, rnetlib::Channel::ptr> channels_;

  int SocketShuffle(const rdd::RDDContext &rdd_ctx);

  int VerbsShuffle(const rdd::RDDContext &rdd_ctx);

  bool IsLocalPartition(uint64_t gpid) const { return gpid2lpid_.find(gpid) != gpid2lpid_.end(); }
};

} // namespace shuffle
} // namespace pmr

#endif // PMR_SHUFFLE_IN_MEMORY_SHUFFLE_SERVICE_H_
