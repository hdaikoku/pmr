#ifndef PMR_SHUFFLE_ON_DISK_SHUFFLE_SERVICE_H_
#define PMR_SHUFFLE_ON_DISK_SHUFFLE_SERVICE_H_

#include <string>
#include <unordered_map>
#include <vector>

#include "pmr/shuffle/shuffle_service.h"

#define DIR_PREFIX "shuffle_blk_"

namespace pmr {
namespace shuffle {

class OnDiskShuffleService : public ShuffleService {
 public:
  explicit OnDiskShuffleService(const std::string &tmp_dir);
  ~OnDiskShuffleService() override = default;

  void Init(uint64_t executor_id) override {}

  void InitBlockBuffer(uint64_t num_partitions, const std::vector<uint64_t> &local_gpids) override;

  int Start(const rdd::RDDContext &rdd_ctx) override;

  void PutBlock(uint64_t src_gpid, uint64_t dst_gpid, Block blk) override;

  Block GetBlock(uint64_t src_gpid, uint64_t dst_gpid) override;

  uint16_t GetBindPort() const override { return 0; }

 private:
  std::string tmp_dir_;
  uint64_t num_local_partitions_;
  std::unordered_map<uint64_t, uint64_t> gpid2lpid_;
  std::vector<Block> block_buffers_;

  int ShuffleWrite(uint64_t src_gpid, uint64_t dst_gpid, Block blk) const;

  Block ShuffleRead(uint64_t src_gpid, uint64_t dst_gpid) const;

  std::string GetShuffleFilePath(uint64_t src_gpid, uint64_t dst_gpid) const;
};

} // namespace shuffle
} // namespace pmr

#endif // PMR_SHUFFLE_ON_DISK_SHUFFLE_SERVICE_H_
