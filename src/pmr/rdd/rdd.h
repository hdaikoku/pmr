#ifndef PMR_RDD_RDD_H_
#define PMR_RDD_RDD_H_

#include <string>

#include "pmr/rdd/rdd_context.h"
#include "pmr/utils/logger.h"

namespace pmr {
namespace rdd {

class RDD : public Logger {
 public:
  RDD(std::shared_ptr<RDDContext> context,
      uint64_t partition_gid,
      uint64_t partition_lid,
      const std::string &log_tag = "RDD")
      : Logger(log_tag), context_(std::move(context)), partition_gid_(partition_gid), partition_lid_(partition_lid) {};

  virtual ~RDD() = default;

  virtual void Compute() = 0;

  virtual void Print() const = 0;

  uint64_t GetNumPartitions() const { return context_->num_partitions; }

  const std::vector<uint64_t> &GetLocalGPIDs(uint64_t executor_id) const {
    return context_->partitions_by_executor[executor_id];
  }

  const RDDContext &GetContext() const { return *context_; }

  uint64_t GetGPID() const { return partition_gid_; }

  uint64_t GetLPID() const { return partition_lid_; }

 protected:
  std::shared_ptr<RDDContext> context_;
  uint64_t partition_gid_;
  uint64_t partition_lid_;
};

} // namespace rdd
} // namespace pmr

#endif // PMR_RDD_RDD_H_
