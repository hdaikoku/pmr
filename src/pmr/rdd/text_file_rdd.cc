#include "text_file_rdd.h"

#include <fstream>

namespace pmr {
namespace rdd {

TextFileRDD::TextFileRDD(std::shared_ptr<RDDContext> context,
                         uint64_t partition_gid,
                         uint64_t partition_lid,
                         const std::string &file_path,
                         const TextFileIndex &index)
    : KeyValueRDD(context, partition_gid, partition_lid, "TextFileRDD"),
      file_path_(file_path), offset_(index.offset_), size_(index.size_) { Compute(); }

void TextFileRDD::Compute() {
  std::unique_ptr<char[]> buf(new char[size_ + 1]);
  std::ifstream ifs(file_path_);
  if (!ifs) {
    LogError("failed to open file: " + file_path_);
    return;
  }

  ifs.seekg(offset_);
  ifs.read(buf.get(), size_);
  buf[size_] = '\0';
  ifs.close();

  char *save_ptr;
  auto offset = offset_;
  auto line = strtok_r(buf.get(), "\n", &save_ptr);
  while (line != nullptr) {
    auto len = std::char_traits<char>::length(line);
    buffer_->Insert(std::make_pair(offset, std::string(line, len)));
    offset += (len + 1);
    line = strtok_r(nullptr, "\n", &save_ptr);
  }
}

} // namespace rdd
} // namespace pmr
