#include "pmr/rdd/key_values_rdd.h"

#include "pmr/storage/combined_pair_buffer.h"

namespace pmr {
namespace rdd {

template <typename K, typename V>
KeyValuesRDD<K, V>::KeyValuesRDD(std::shared_ptr<RDDContext> context,
                                 uint64_t partition_gid,
                                 uint64_t partition_lid,
                                 std::unique_ptr<storage::PairBuffer<K, V>> buffer,
                                 const std::string &log_tag)
    : RDD(context, partition_gid, partition_lid, log_tag), buffer_(std::move(buffer)) {}

template <typename K, typename V>
std::unique_ptr<RDD> KeyValuesRDD<K, V>::Reduce(const Reducer<V> *reducer) {
  std::unique_ptr<storage::PairBuffer<K, V>> buf(new storage::CombinedPairBuffer<K, V>(reducer));
  buf->ReadBlocks(partition_gid_, context_->num_partitions);

  return std::unique_ptr<RDD>(new KeyValueRDD<K, V>(
      context_, partition_gid_, partition_lid_, std::move(buf)
  ));
}

template <typename K, typename V>
void KeyValuesRDD<K, V>::Compute() {}

template <typename K, typename V>
void KeyValuesRDD<K, V>::Print() const {
  buffer_->ForEach([&](const std::pair<K, V> &kv) {
    std::cout << "(" << kv.first << ", " << kv.second << ")" << std::endl;
  });
}

} // namespace rdd
} // namespace pmr
