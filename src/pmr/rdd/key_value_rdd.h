#ifndef PMR_RDD_KEY_VALUE_RDD_H_
#define PMR_RDD_KEY_VALUE_RDD_H_

#include <iostream>

#include "pmr/mapper.h"
#include "pmr/reducer.h"
#include "pmr/rdd/rdd.h"
#include "pmr/storage/pair_buffer.h"

namespace pmr {
namespace rdd {

template <typename K, typename V>
class KeyValueRDD : public RDD {
 public:
  KeyValueRDD(std::shared_ptr<RDDContext> context,
              uint64_t partition_gid,
              uint64_t partition_lid,
              const std::string &log_tag = "KeyValueRDD");
  KeyValueRDD(std::shared_ptr<RDDContext> context,
              uint64_t partition_gid,
              uint64_t partition_lid,
              std::unique_ptr<storage::PairBuffer<K, V>> buffer,
              const std::string &log_tag = "KeyValueRDD");

  template <typename NK, typename NV>
  std::unique_ptr<RDD> Map(const Mapper<NK, NV, K, V> *mapper, const Reducer<NV> *combiner);

  void Compute() override;

  void Print() const override;

 protected:
  std::unique_ptr<storage::PairBuffer<K, V>> buffer_;
};

} // namespace rdd
} // namespace pmr

#include "pmr/rdd/key_value_rdd.cc"

#endif // PMR_RDD_KEY_VALUE_RDD_H_
