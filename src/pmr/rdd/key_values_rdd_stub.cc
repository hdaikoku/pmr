#include "pmr/rdd/key_values_rdd_stub.h"

#include "pmr/pmr_context.h"

namespace pmr {
namespace rdd {

KeyValuesRDDStub::KeyValuesRDDStub(PMRContext &pc, const std::string &log_tag) : RDDStub(pc, log_tag) {}

KeyValuesRDDStub::KeyValuesRDDStub(std::shared_ptr<RDDContext> context, PMRContext &pc, const std::string &log_tag)
    : RDDStub(context, pc, log_tag) {}

std::unique_ptr<RDDStub> KeyValuesRDDStub::Reduce(const std::string &lib_reducer) {
  std::string dl_reducer_path(realpath(lib_reducer.c_str(), nullptr));

  std::unique_ptr<RDDStub> new_rdd(new KeyValuesRDDStub(context_, pc_));
  const auto &partitions_by_executor = context_->partitions_by_executor;
  std::vector<msgpack::rpc::future> fs;
  for (const auto &p : partitions_by_executor) {
    fs.emplace_back(pc_.ExecutorRPC(p.first, "reduce_rdd", id_, new_rdd->GetID(), dl_reducer_path));
  }

  for (auto &f : fs) {
    if (f.get<int>() != 0) {
      LogError("failed to reduce RDD " + std::to_string(id_));
    }
  }

  return std::move(new_rdd);
}

void KeyValuesRDDStub::GroupBy() {

}

void KeyValuesRDDStub::Shuffle() {

}

} // namespace rdd
} // namespace pmr
