#include "pmr/rdd/text_file_rdd_stub.h"

#include <fstream>

#include "pmr/text_file_index.h"

namespace pmr {
namespace rdd {

TextFileRDDStub::TextFileRDDStub(PMRContext &pc, const std::string &file_path, const std::string &log_tag)
    : KeyValueRDDStub(pc, log_tag) {
  std::unordered_map<uint64_t, std::vector<TextFileIndex>> indices;
  uint64_t next_executor = 0;
  auto num_executors = pc.NumExecutors();
  auto chunk_size = (1 << 27); // FIXME: default chunk size should be user-configurable

  std::ifstream ifs(file_path);
  if (!ifs) {
    LogError("failed to open file: " + file_path);
    return;
  }

  int64_t file_size = ifs.seekg(0, ifs.end).tellg();
  ifs.seekg(0, ifs.beg);

  uint64_t num_partitions = 0;
  while (!ifs.eof()) {
    uint64_t owner = next_executor++ % num_executors;
    int64_t offset = ifs.tellg();

    if ((file_size - offset) < chunk_size) {
      if (file_size > offset) {
        indices[owner].emplace_back(num_partitions, offset, (file_size - offset));
        SetPartitionOwner(owner, num_partitions++);
      }
      break;
    }

    ifs.seekg(chunk_size, ifs.cur);

    if (!ifs.eof()) {
      ifs.ignore(chunk_size, '\n');
    }
    int64_t end = ifs.tellg();

    indices[owner].emplace_back(num_partitions, offset, (end - offset));
    SetPartitionOwner(owner, num_partitions++);
  }
  ifs.close();

  std::vector<msgpack::rpc::future> futures;
  auto rdd_id = GetRDDID();
  for (const auto &idx : indices) {
    futures.push_back(pc.ExecutorRPC(idx.first, "text_file", rdd_id, num_partitions,
                                     GetPartitionDistribution(), file_path, idx.second));
  }

  for (auto &f : futures) {
    if (f.get<int>()) {
      LogError("failed to distribute file: " + file_path);
      // FIXME: re-distribute failed partition
    }
  }
}

} // namespace rdd
} // namespace pmr