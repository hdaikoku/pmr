#ifndef PMR_RDD_UDF_H_
#define PMR_RDD_UDF_H_

#include <dlfcn.h>

#include <string>

namespace pmr {
namespace rdd {

class UDF {
 public:
  explicit UDF(const std::string &dl_path);
  virtual ~UDF();

  template <typename T>
  std::unique_ptr<T> Load();

 private:
  void *lib_;
};

UDF::UDF(const std::string &dl_path) {
  lib_ = dlopen(dl_path.c_str(), RTLD_LAZY);
}

UDF::~UDF() {
  if (lib_) {
    dlclose(lib_);
  }
}

template <typename T>
std::unique_ptr<T> UDF::Load() {
  if (!lib_) {
    return nullptr;
  }

  const auto func = dlsym(lib_, "NewInstance");
  if (dlerror()) {
    dlclose(lib_);
    return nullptr;
  }

  return reinterpret_cast<std::unique_ptr<T> (*)()>(func)();
}

} // namespace rdd
} // namespace pmr

#endif // PMR_RDD_UDF_H_
