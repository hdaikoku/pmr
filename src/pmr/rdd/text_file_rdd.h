#ifndef PMR_RDD_TEXT_FILE_RDD_H_
#define PMR_RDD_TEXT_FILE_RDD_H_

#include <pmr/text_file_index.h>
#include "pmr/rdd/key_value_rdd.h"

namespace pmr {
namespace rdd {

class TextFileRDD : public KeyValueRDD<long long int, std::string> {
 public:
  TextFileRDD(std::shared_ptr<RDDContext> context,
              uint64_t partition_gid,
              uint64_t partition_lid,
              const std::string &file_path,
              const TextFileIndex &index);

  void Compute() override;

 private:
  std::string file_path_;
  int64_t offset_;
  int32_t size_;
};

} // namespace rdd
} // namespace pmr

#endif // PMR_RDD_TEXT_FILE_RDD_H_
