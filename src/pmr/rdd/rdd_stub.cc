#include "pmr/rdd/rdd_stub.h"

#include "pmr/pmr_context.h"

namespace pmr {
namespace rdd {

RDDStub::RDDStub(PMRContext &pc, const std::string &log_tag)
    : RDDStub(std::make_shared<RDDContext>(), pc, log_tag) {}

RDDStub::RDDStub(std::shared_ptr<RDDContext> context, PMRContext &pc, const std::string &log_tag)
    : Logger(log_tag), context_(std::move(context)), pc_(pc), id_(pc.NewRDDID()) {}

RDDStub::~RDDStub() {
  const auto &partitions_by_executor = context_->partitions_by_executor;
  std::vector<msgpack::rpc::future> fs;

  for (const auto &p : partitions_by_executor) {
    fs.push_back(pc_.ExecutorRPC(p.first, "remove_rdd", id_));
  }

  for (auto &f : fs) {
    if (f.get<int>() != 0) {
      LogError("failed to clear RDD " + std::to_string(id_));
    }
  }
}

std::unique_ptr<RDDStub> RDDStub::Map(const std::string &lib_mapper) {
  return Map(lib_mapper, "");
}

std::unique_ptr<RDDStub> RDDStub::Map(const std::string &lib_mapper, const std::string &lib_combiner) {
  std::string dl_mapper_path(realpath(lib_mapper.c_str(), nullptr));
  std::string dl_combiner_path;
  if (!lib_combiner.empty()) {
    dl_combiner_path.assign(realpath(lib_combiner.c_str(), nullptr));
  }

  std::unique_ptr<RDDStub> new_rdd(new RDDStub(context_, pc_));
  const auto &partitions_by_executor = context_->partitions_by_executor;
  const auto &new_rdd_id = new_rdd->GetID();
  std::vector<msgpack::rpc::future> fs;

  auto beg = std::chrono::steady_clock::now();
  for (const auto &p : partitions_by_executor) {
    fs.emplace_back(pc_.ExecutorRPC(p.first, "map_rdd", id_, new_rdd_id, dl_mapper_path, dl_combiner_path));
  }

  for (auto &f : fs) {
    if (f.get<int>() != 0) {
      LogError("failed to map RDD " + std::to_string(id_));
    }
  }
  auto end = std::chrono::steady_clock::now();

  auto dur = std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - beg).count() / 1000.);
  LogInfo("Map: " + dur + " [seconds]");

  return std::move(new_rdd);
}

std::unique_ptr<RDDStub> RDDStub::Reduce(const std::string &lib_reducer) {
  Shuffle();

  std::string dl_reducer_path(realpath(lib_reducer.c_str(), nullptr));

  std::unique_ptr<RDDStub> new_rdd(new RDDStub(context_, pc_));
  const auto &partitions_by_executor = context_->partitions_by_executor;
  std::vector<msgpack::rpc::future> fs;

  auto beg = std::chrono::steady_clock::now();
  for (const auto &p : partitions_by_executor) {
    fs.emplace_back(pc_.ExecutorRPC(p.first, "reduce_rdd", id_, new_rdd->GetID(), dl_reducer_path));
  }

  for (auto &f : fs) {
    if (f.get<int>() != 0) {
      LogError("failed to reduce RDD " + std::to_string(id_));
    }
  }
  auto end = std::chrono::steady_clock::now();

  auto dur = std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - beg).count() / 1000.);
  LogInfo("Reduce: " + dur + " [seconds]");

  return std::move(new_rdd);
}

void RDDStub::Print() const {
  const auto &partitions_by_executor = context_->partitions_by_executor;
  std::vector<msgpack::rpc::future> fs;

  for (const auto &p : partitions_by_executor) {
    fs.emplace_back(pc_.ExecutorRPC(p.first, "print_rdd", id_));
  }

  for (auto &f : fs) {
    if (f.get<int>() != 0) {
      LogError("failed to print RDD " + std::to_string(id_));
    }
  }
}

void RDDStub::Shuffle() {
  const auto &partitions_by_executor = context_->partitions_by_executor;
  std::vector<msgpack::rpc::future> fs;

  auto beg = std::chrono::steady_clock::now();
  for (const auto &p : partitions_by_executor) {
    fs.emplace_back(pc_.ExecutorRPC(p.first, "shuffle_rdd", id_));
  }

  for (auto &f : fs) {
    if (f.get<int>() != 0) {
      LogError("failed to shuffle RDD " + std::to_string(id_));
    }
  }
  auto end = std::chrono::steady_clock::now();

  auto dur = std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - beg).count() / 1000.);
  LogInfo("Shuffle: " + dur + " [seconds]");
}

} // namespace rdd
} // namespace pmr
