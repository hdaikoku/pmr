//
// Created by Harunobu Daikoku on 2017/10/05.
//

#ifndef PMR_RDD_KEY_VALUES_RDD_H_
#define PMR_RDD_KEY_VALUES_RDD_H_

#include "pmr/reducer.h"
#include "pmr/rdd/rdd.h"

namespace pmr {
namespace rdd {

template <typename K, typename V>
class KeyValuesRDD : public RDD {
 public:
  KeyValuesRDD(std::shared_ptr<RDDContext> context,
               uint64_t partition_gid,
               uint64_t partition_lid,
               std::unique_ptr<storage::PairBuffer<K, V>> buffer,
               const std::string &log_tag = "KeyValuesRDD");

  std::unique_ptr<RDD> Reduce(const Reducer<V> *reducer);

  void Compute() override;

  void Print() const override;

 private:
  std::unique_ptr<storage::PairBuffer<K, V>> buffer_;
};

} // namespace rdd
} // namespace pmr

#include "pmr/rdd/key_values_rdd.cc"

#endif // PMR_RDD_KEY_VALUES_RDD_H_
