#ifndef UTILS_ARG_PARSER_H_
#define UTILS_ARG_PARSER_H_

#include <iomanip>
#include <iostream>
#include <getopt.h>
#include <string>
#include <unordered_map>
#include <vector>

class ArgParser {
 public:
  ArgParser() = default;
  ~ArgParser() = default;

  void PrintOptions() const {
    std::cerr << "Options:" << std::endl;
    for (const auto &opt : opts_) {
      std::string opt_str;
      opt_str += "-";
      opt_str += static_cast<char>(opt.val);
      opt_str += ",--";
      opt_str += opt.name;
      if (opt.has_arg) {
        opt_str += "=<value>";
      }
      std::cerr << "\t" << std::left << std::setw(30) << opt_str << desc_strs_.at(opt.val) << std::endl;
    }
  }

  void AddOption(int short_opt, const std::string &long_opt, const std::string &desc, bool require_val) {
    long_opts_.emplace(std::make_pair(short_opt, long_opt));
    desc_strs_.emplace(std::make_pair(short_opt, desc));
    opts_.push_back({long_opts_[short_opt].c_str(), require_val ? required_argument : no_argument, NULL, short_opt});
  }

  int Parse(int argc, char *argv[]) {
    std::string opt_str;
    for (const auto &opt : opts_) {
      opt_str.push_back(static_cast<char>(opt.val));
      if (opt.has_arg) {
        opt_str.push_back(':');
      }
    }

    opts_.push_back({0, 0, 0, 0});

    int short_opt;
    while ((short_opt = getopt_long(argc, argv, opt_str.c_str(), opts_.data(), NULL)) != -1) {
      for (const auto &opt : opts_) {
        if (opt.val == 0 || short_opt == '?') {
          // error
          opts_.pop_back();
          return -1;
        } else if (opt.val == short_opt) {
          vals_.emplace(std::make_pair(opt.val, opt.has_arg ? reinterpret_cast<uintptr_t>(optarg) : 1));
          break;
        }
      }
    }

    opts_.pop_back();

    return optind;
  }

  bool HasOpt(int short_opt) const {
    return (vals_.find(short_opt) != vals_.end());
  }

  const char *GetValStr(int short_opt) const {
    return reinterpret_cast<const char *>(vals_.at(short_opt));
  }

 private:
  std::vector<struct option> opts_;
  std::unordered_map<int, std::string> long_opts_;
  std::unordered_map<int, std::string> desc_strs_;
  std::unordered_map<int, uintptr_t> vals_;
};

#endif // UTILS_ARG_PARSER_H_
