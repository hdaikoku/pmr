#ifndef UTILS_LOGGER_H_
#define UTILS_LOGGER_H_

#include <spdlog/spdlog.h>

class Logger {
 public:
  Logger(const std::string &log_tag)
      : logger_(std::make_shared<spdlog::logger>(log_tag, spdlog::sinks::stderr_sink_mt::instance())) {
    logger_->set_pattern("[%l] %C/%m/%d %T %n: %v");
#ifndef NDEBUG
    logger_->set_level(spdlog::level::debug);
#endif // NDEBUG
  }

 protected:
  void LogError(const std::string &msg) const { logger_->error(msg); }

  void LogError(int error_num) const { logger_->error(spdlog::details::os::errno_str(error_num)); }

  void LogInfo(const std::string &msg) const { logger_->info(msg); }

  void LogDebug(const std::string &msg) const { logger_->debug(msg); }

  const std::string &GetLogTag() const { return logger_->name(); }

 private:
  std::shared_ptr<spdlog::logger> logger_;
};

#endif // UTILS_LOGGER_H_
