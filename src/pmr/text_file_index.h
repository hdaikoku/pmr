#ifndef PMR_TEXT_FILE_INDEX_H_
#define PMR_TEXT_FILE_INDEX_H_

#include <msgpack.hpp>

namespace pmr {

struct TextFileIndex {
  TextFileIndex() = default;
  TextFileIndex(uint64_t partition_id, int64_t offset, int32_t size)
      : partition_id_(partition_id), offset_(offset), size_(size) {}

  uint64_t partition_id_;
  int64_t offset_;
  int32_t size_;

  MSGPACK_DEFINE(partition_id_, offset_, size_);
};

} // namespace pmr

#endif // PMR_TEXT_FILE_INDEX_H_
