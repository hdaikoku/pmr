#!/usr/bin/env bash

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

"${PMR_HOME}/sbin/pmr-daemon.sh" stop "${PMR_HOME}/bin/pmr_master" 0
