#!/usr/bin/env bash

if [ $# -lt 1 ]; then
    echo "Usage: ${0} [path_to_app] arguments..."
    exit 1
fi

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

. "${PMR_HOME}/sbin/load-pmr-env.sh"

if [ -z "${PMR_MASTER_HOST}" ]; then
    export PMR_MASTER_HOST="localhost"
fi

if [ -z "${PMR_MASTER_PORT}" ]; then
    export PMR_MASTER_PORT=50090
fi

"$@"
