#!/usr/bin/env bash

if [ $# -lt 1 ]; then
    echo "Usage: ${0} [commands...]"
    exit 1
fi

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

. "${PMR_HOME}/sbin/load-pmr-env.sh"

if [ -f "${PMR_HOME}/conf/workers" ]; then
    workers=`cat "${PMR_HOME}/conf/workers"`
else
    workers="localhost"
fi

if [ -z "${PMR_SSH_CMD}" ]; then
    PMR_SSH_CMD="ssh -f -o StrictHostKeyChecking=no"
fi

for worker in `echo "${workers}" | sed  "s/#.*$//;/^$/d"`; do
    ${PMR_SSH_CMD} "${worker}" unset PMR_ENV_LOADED \; $"${@// /\\ }" 2>&1 | sed "s/^/${worker}: /" &
done

wait
