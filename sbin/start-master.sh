#!/usr/bin/env bash

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

. "${PMR_HOME}/sbin/load-pmr-env.sh"

if [ -z "${PMR_MASTER_HOST}" ]; then
    PMR_MASTER_HOST="localhost"
fi

if [ -z "${PMR_MASTER_PORT}" ]; then
    PMR_MASTER_PORT=50090
fi

"${PMR_HOME}/sbin/pmr-daemon.sh" start "${PMR_HOME}/bin/pmr_master" 0 -a ${PMR_MASTER_HOST} ${PMR_MASTER_PORT}
