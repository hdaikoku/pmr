#!/usr/bin/env bash

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

# start workers
"${PMR_HOME}/sbin"/stop-workers.sh

# start master
"${PMR_HOME}/sbin"/stop-master.sh
