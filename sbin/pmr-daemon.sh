#!/usr/bin/env bash

usage="Usage: ${0} (start/stop) [command] [sequence_num] [arguments...]"

if [ $# -lt 3 ]; then
    echo "${usage}"
    exit 1
fi

opt="${1}"
shift
cmd="${1}"
cmd_name=$(basename "${cmd}")
shift
seq="${1}"
shift

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

. "${PMR_HOME}/sbin/load-pmr-env.sh"

if [ -z "${PMR_LOG_DIR}" ]; then
    PMR_LOG_DIR="${PMR_HOME}/logs"
fi
log_dir="${PMR_LOG_DIR}/pmr-${USER}-${cmd_name}-${seq}-`hostname`"
mkdir -p "${log_dir}"

pid_file="/tmp/pmr-${USER}-${cmd_name}-${seq}.pid"

run_command() {
    if [ -f ${pid_file} ]; then
        target_pid="$(cat "${pid_file}")"
        if [[ $(ps -p "${target_pid}" -o comm=) =~ "${cmd_name}" ]]; then
            echo "${cmd_name} already running (PID: ${target_pid}).  Stop it first."
            exit 1
        fi
    fi

    echo "starting ${cmd_name}..."
    nohup -- "${cmd}" "$@" 1>"${log_dir}/stdout" 2>"${log_dir}/stderr" < /dev/null &
    echo "$!" > "${pid_file}"
}

stop_command() {
    if [ -f ${pid_file} ]; then
        target_pid="$(cat "${pid_file}")"
        if [[ $(ps -p "${target_pid}" -o comm=) =~ "${cmd_name}" ]]; then
            echo "stopping ${cmd_name}..."
            kill "${target_pid}" && rm -f "${pid_file}"
        else
            echo "no ${cmd_name} to stop"
        fi
    else
        echo "no ${cmd_name} to stop"
    fi
}

case ${opt} in
    "start")
        run_command "$@";;

    "stop")
        stop_command;;

    *)
        echo "${usage}"
        exit 1;;

esac
