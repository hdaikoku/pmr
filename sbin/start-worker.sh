#!/usr/bin/env bash

if [ $# -lt 2 ]; then
    echo "Usage: ${0} [master_addr] [master_port]"
    exit 1
fi

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

. "${PMR_HOME}/sbin/load-pmr-env.sh"

if [ -z "${PMR_LOCAL_IP}" ]; then
    PMR_LOCAL_IP="`hostname -f`"
fi

if [ -z "${PMR_WORKER_INSTANCES}" ]; then
    PMR_WORKER_INSTANCES=1
fi

for ((i=0; i<"${PMR_WORKER_INSTANCES}"; i++)); do
    "${PMR_HOME}/sbin/pmr-daemon.sh" start "${PMR_HOME}/bin/pmr_worker" "$i" -a ${PMR_LOCAL_IP} "${1}" "${2}"
done
