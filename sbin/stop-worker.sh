#!/usr/bin/env bash

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

. "${PMR_HOME}/sbin/load-pmr-env.sh"

if [ -z "${PMR_WORKER_INSTANCES}" ]; then
    PMR_WORKER_INSTANCES=1
fi

for ((i=0; i<"${PMR_WORKER_INSTANCES}"; i++)); do
    "${PMR_HOME}/sbin/pmr-daemon.sh" stop "${PMR_HOME}/bin/pmr_worker" "$i"
done
