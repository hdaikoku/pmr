#!/usr/bin/env bash

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

if [ -z "${PMR_ENV_LOADED}" ]; then
    export PMR_ENV_LOADED=1

    user_conf_dir="${PMR_CONF_DIR:-"${PMR_HOME}/conf"}"

    if [ -f "${user_conf_dir}/pmr-env.sh" ]; then
        set -a
        . "${user_conf_dir}/pmr-env.sh"
        set +a
    fi
fi
