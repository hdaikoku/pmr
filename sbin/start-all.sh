#!/usr/bin/env bash

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

# start master
"${PMR_HOME}/sbin"/start-master.sh

# start workers
"${PMR_HOME}/sbin"/start-workers.sh
