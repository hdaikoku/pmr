#!/usr/bin/env bash

if [ -z "${PMR_HOME}" ]; then
    export PMR_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

. "${PMR_HOME}/sbin/load-pmr-env.sh"

"${PMR_HOME}/sbin/workers.sh" "${PMR_HOME}/sbin/stop-worker.sh"
