#ifndef PMR_REDUCER_H_
#define PMR_REDUCER_H_

#include <memory>

namespace pmr {

template <typename V>
class Reducer {
 public:
  using Type = typename std::unique_ptr<Reducer<V>>;

  Reducer() = default;
  virtual ~Reducer() = default;

  virtual V Reduce(const V &v1, const V &v2) const = 0;
};

} // namespace pmr

#endif // PMR_REDUCER_H_
