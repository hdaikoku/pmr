#ifndef PMR_RDD_RDD_STUB_H_
#define PMR_RDD_RDD_STUB_H_

#include "pmr/rdd/rdd_context.h"
#include "pmr/utils/logger.h"

namespace pmr {
// forward declaration of PMRContext
class PMRContext;

namespace rdd {

class RDDStub : public Logger {
 public:
  explicit RDDStub(PMRContext &pc, const std::string &log_tag = "RDDStub");
  RDDStub(std::shared_ptr<RDDContext> context, PMRContext &pc, const std::string &log_tag = "RDDStub");

  virtual ~RDDStub();

  std::unique_ptr<RDDStub> Map(const std::string &lib_mapper);

  std::unique_ptr<RDDStub> Map(const std::string &lib_mapper, const std::string &lib_combiner);

  std::unique_ptr<RDDStub> Reduce(const std::string &lib_reducer);

  void Print() const;

  void SetPartitionOwner(uint64_t executor_id, uint64_t partition_id) {
    context_->partitions_by_executor[executor_id].emplace_back(partition_id);
  }

  void SetNumPartitions(uint64_t num_partitions) { context_->num_partitions = num_partitions; }

  const RDDContext &GetContext() const { return *context_; }

  int GetID() const { return id_; }

 private:
  PMRContext &pc_;
  std::shared_ptr<RDDContext> context_;
  int id_;

  void Shuffle();
};

} // namespace rdd
} // namespace pmr

#endif // PMR_RDD_RDD_STUB_H_
