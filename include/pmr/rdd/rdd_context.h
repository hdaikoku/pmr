#ifndef PMR_RDD_RDD_CONTEXT_H_
#define PMR_RDD_RDD_CONTEXT_H_

#include <unordered_map>
#include <vector>

#include <msgpack.hpp>

namespace pmr {
namespace rdd {

struct RDDContext {
  uint64_t num_partitions;
  std::unordered_map<uint64_t, std::vector<uint64_t>> partitions_by_executor;

  MSGPACK_DEFINE(num_partitions, partitions_by_executor);
};

} // namespace rdd
} // namespace pmr

#endif // PMR_RDD_RDD_CONTEXT_H_
