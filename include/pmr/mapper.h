#ifndef PMR_MAPPER_H_
#define PMR_MAPPER_H_

#include <memory>
#include <vector>

namespace pmr {

template <typename NK, typename NV, typename K, typename V>
class Mapper {
 public:
  using Type = typename std::unique_ptr<Mapper<NK, NV, K, V>>;
  using KVs = std::vector<std::pair<NK, NV>>;

  Mapper() = default;
  virtual ~Mapper() = default;

  virtual KVs Map(const K &key, const V &value) const = 0;
};

} // namespace pmr

#endif // PMR_MAPPER_H_
