#ifndef PMR_PMR_CONTEXT_H_
#define PMR_PMR_CONTEXT_H_

#include <jubatus/msgpack/rpc/session_pool.h>

#include "pmr/executor_info.h"
#include "pmr/rdd/rdd_stub.h"
#include "pmr/utils/logger.h"

namespace pmr {

class PMRContext : public Logger {
 public:
  PMRContext();

  virtual ~PMRContext();

  std::unique_ptr<rdd::RDDStub> TextFile(const std::string &file_path);

  int NewRDDID() { return new_rdd_id_++; }

  msgpack::rpc::future MasterRPC(const std::string &func) {
    return sp_.get_session(master_addr_, master_port_).call(func);
  }

  template <typename A1>
  msgpack::rpc::future MasterRPC(const std::string &func, const A1 &a1) {
    return sp_.get_session(master_addr_, master_port_).call(func, a1);
  }

  msgpack::rpc::future ExecutorRPC(uint64_t executor_id, const std::string &func) {
    return sp_.get_session(executors_[executor_id].rpc_addr, executors_[executor_id].rpc_port).call(func);
  }

  template <typename A1>
  msgpack::rpc::future ExecutorRPC(uint64_t executor_id, const std::string &func, const A1 &a1) {
    return sp_.get_session(executors_[executor_id].rpc_addr, executors_[executor_id].rpc_port).call(func, a1);
  }

  template <typename A1, typename A2, typename A3>
  msgpack::rpc::future ExecutorRPC(uint64_t executor_id, const std::string &func,
                                   const A1 &a1, const A2 &a2, const A3 &a3) {
    return sp_.get_session(executors_[executor_id].rpc_addr, executors_[executor_id].rpc_port)
        .call(func, a1, a2, a3);
  }

  template <typename A1, typename A2, typename A3, typename A4>
  msgpack::rpc::future ExecutorRPC(uint64_t executor_id, const std::string &func,
                                   const A1 &a1, const A2 &a2, const A3 &a3, const A4 &a4) {
    return sp_.get_session(executors_[executor_id].rpc_addr, executors_[executor_id].rpc_port)
        .call(func, a1, a2, a3, a4);
  }

  template <typename A1, typename A2, typename A3, typename A4, typename A5>
  msgpack::rpc::future ExecutorRPC(uint64_t executor_id, const std::string &func,
                                   const A1 &a1, const A2 &a2, const A3 &a3, const A4 &a4, const A5 &a5) {
    return sp_.get_session(executors_[executor_id].rpc_addr, executors_[executor_id].rpc_port)
        .call(func, a1, a2, a3, a4, a5);
  }

 private:
  std::string master_addr_;
  uint16_t master_port_;
  msgpack::rpc::session_pool sp_;
  std::unordered_map<uint64_t, ExecutorInfo> executors_;
  int new_rdd_id_;

  void SetTimeout(uint64_t executor_id, unsigned int timeout_sec) {
    sp_.get_session(executors_[executor_id].rpc_addr, executors_[executor_id].rpc_port).set_timeout(timeout_sec);
  }
};

} // namespace pmr

#endif // PMR_PMR_CONTEXT_H_
